package Controlador;

import modelo.*;
import Vista.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Controlador implements ActionListener {
    private dlgVentas vista1;
    private dlgRegistros vista2;
    private Ventas ventas;
    private dbManejador Manejador;
    private dbVentas db;
    private boolean isInsertar = true;

    public Controlador(dlgVentas vista1, dlgRegistros vista2, Ventas ventas, dbManejador Manejador, dbVentas db) {
        this.vista1 = vista1;
        this.vista2 = vista2;
        this.ventas = ventas;
        this.Manejador = Manejador;
        this.db = db;

        vista1.btnNuevo.addActionListener(this);
        vista1.btnRealizar.addActionListener(this);
        vista1.btnGuardar.addActionListener(this);
        vista1.btnBuscar.addActionListener(this);
        vista1.btnMostrar.addActionListener(this);
        vista2.btnMostrarT.addActionListener(this);
    }
    private void iniciarVista() {
        try {
            ArrayList<Ventas> listaVentas = db.listar();
            DefaultTableModel modelo = new DefaultTableModel();
            modelo.addColumn("ID");
            modelo.addColumn("Código");
            modelo.addColumn("Tipo");
            modelo.addColumn("Precio");
            modelo.addColumn("Cantidad");
            modelo.addColumn("Total");
            for (Ventas venta : listaVentas) {
                Object[] fila = {venta.getId(), venta.getCodigo(), venta.getTipo(), venta.getPrecio(), venta.getCantidad(), venta.getTotal()};
                modelo.addRow(fila);
            }
            vista2.jtbRegistros.setModel(modelo);
            vista1.setTitle("Registros de Ventas");
            vista1.setSize(600, 600);
            vista1.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista1, "Error al iniciar: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista1.btnNuevo) {
            habilitar();
            isInsertar = true;
        } else if (e.getSource() == vista1.btnRealizar) {
            realizarVenta();
        } else if (e.getSource() == vista1.btnGuardar) {
            if (isInsertar) {
                guardarVenta();
            } else {
                actualizarVenta();
            }
        } else if (e.getSource() == vista1.btnBuscar) {
            buscarVenta();
        } else if (e.getSource() == vista1.btnMostrar) {
            // Mostrar solo la vista dlgRegistros sin cargar la tabla
            vista2.setVisible(true);
            vista2.jtbRegistros.setModel(new DefaultTableModel()); // Limpiar la tabla
        } else if (e.getSource() == vista2.btnMostrarT) {
            // Mostrar la tabla directamente
            mostrarRegistrosEnTabla();
        }
    }
    private void habilitar() {
        vista1.txtCodigo.setEnabled(true);
        vista1.txtCantidad.setEnabled(true);
        vista1.cmbTipos.setEnabled(true);
        vista1.btnRealizar.setEnabled(true);
        vista1.btnMostrar.setEnabled(true);
        vista1.btnGuardar.setEnabled(true);
        vista1.btnBuscar.setEnabled(true);
        vista1.txtCodigo.setText("");
        vista1.txtCantidad.setText("");
        vista1.cmbTipos.setSelectedIndex(0);
        vista1.lblPrecio.setText("");
        vista1.lblTotal.setText("");
         vista1.lblCantidad.setText("");
    }
    private void realizarVenta() {
        int tipo = vista1.cmbTipos.getSelectedIndex();
        double precioPorLitro;
        if (tipo == 1) {
            precioPorLitro = 24.50;
        } else {
            precioPorLitro = 20.50;
        }
        try {
            String cantidadString = vista1.txtCantidad.getText();
            int cantidad = 0;
            if (!cantidadString.isEmpty()) {
                cantidad = Integer.parseInt(cantidadString);
                if (cantidad <= 0) {
                    throw new NumberFormatException();
                }
            } else {
                throw new NumberFormatException();
            }
            double precio = precioPorLitro;
            double total = precio * cantidad;
            vista1.lblPrecio.setText(String.valueOf(precioPorLitro)); 
            vista1.lblCantidad.setText(String.valueOf(cantidad)); 
            vista1.lblTotal.setText(String.valueOf(total));
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(vista1, "Error en el formato de cantidad ingresada.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void guardarVenta() {
        String codigo = vista1.txtCodigo.getText();
        int tipo = vista1.cmbTipos.getSelectedIndex() + 1;
        double precio = Double.parseDouble(vista1.lblPrecio.getText());
        int cantidad = Integer.parseInt(vista1.lblCantidad.getText());
        double total = Double.parseDouble(vista1.lblTotal.getText());
        Ventas venta = new Ventas(0, codigo, tipo, precio, cantidad, total);

        try {
            if (this.Manejador.conectar()) {
                db.insertar(venta);
                JOptionPane.showMessageDialog(vista1, "Venta guardada con éxito");
                habilitar();
                ArrayList<Ventas> listaVentas = db.listar();
                DefaultTableModel modelo = new DefaultTableModel();
                modelo.addColumn("ID");
                modelo.addColumn("Código");
                modelo.addColumn("Tipo");
                modelo.addColumn("Precio");
                modelo.addColumn("Cantidad");
                modelo.addColumn("Total");
                for (Ventas v : listaVentas) {
                    Object[] fila = {v.getId(), v.getCodigo(), v.getTipo(), v.getPrecio(), v.getCantidad(), v.getTotal()};
                    modelo.addRow(fila);
                }
                vista2.jtbRegistros.setModel(modelo);
                this.Manejador.desconectar();
            } else {
                JOptionPane.showMessageDialog(vista1, "No se pudo establecer la conexión con la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista1, "Error al guardar la venta: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void actualizarVenta() {
        int filaSeleccionada = vista2.jtbRegistros.getSelectedRow();
        if (filaSeleccionada == -1) {
            JOptionPane.showMessageDialog(vista1, "Debes seleccionar una venta de la tabla para actualizar.");
            return;
        }
        String codigo = vista1.txtCodigo.getText();
        int tipo = vista1.cmbTipos.getSelectedIndex() + 1;
        double precio = Double.parseDouble(vista1.lblPrecio.getText());
        int cantidad = Integer.parseInt(vista1.txtCantidad.getText());
        double total = Double.parseDouble(vista1.lblTotal.getText());
        int Id = (int) vista2.jtbRegistros.getValueAt(filaSeleccionada, 0);

        Ventas venta = new Ventas(Id, codigo, tipo, precio, cantidad, total);
        try {
            if (this.Manejador.conectar()) {
                db.actualizar(venta);
                JOptionPane.showMessageDialog(vista1, "Venta actualizada con éxito");
                habilitar();
                vista2.jtbRegistros.setModel((TableModel) db.listar());
                this.Manejador.desconectar();
            } else {
                JOptionPane.showMessageDialog(vista1, "No se pudo establecer la conexión con la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista1, "Error al actualizar la venta: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void buscarVenta() {
        String codigoBuscado = vista1.txtCodigo.getText();
        try {
            if (this.Manejador.conectar()) {
                Ventas ventaEncontrada = (Ventas) db.buscar(codigoBuscado);
                if (ventaEncontrada != null) {
                    vista1.txtCodigo.setText(ventaEncontrada.getCodigo());
                    vista1.cmbTipos.setSelectedIndex(ventaEncontrada.getTipo() - 1);
                    vista1.lblPrecio.setText(String.valueOf(ventaEncontrada.getPrecio()));
                    vista1.txtCantidad.setText(String.valueOf(ventaEncontrada.getCantidad()));
                    vista1.lblCantidad.setText(String.valueOf(ventaEncontrada.getCantidad()));
                    vista1.lblTotal.setText(String.valueOf(ventaEncontrada.getTotal()));
                    vista1.txtCodigo.setEnabled(false);
                    vista1.txtCantidad.setEnabled(true);
                    vista1.cmbTipos.setEnabled(false);
                    isInsertar = false;
                } else {
                    JOptionPane.showMessageDialog(vista1, "Venta no encontrada.");
                    habilitar();
                    isInsertar = true;
                }
                this.Manejador.desconectar();
            } else {
                JOptionPane.showMessageDialog(vista1, "No se pudo establecer la conexión con la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista1, "Error al buscar la venta: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void mostrarVenta() {
        String codigoBuscado = vista1.txtCodigo.getText();
        try {
            if (this.Manejador.conectar()) {
                Ventas ventaEncontrada = (Ventas) db.buscar(codigoBuscado);
                if (ventaEncontrada != null) {
                    vista1.txtCodigo.setText(ventaEncontrada.getCodigo());
                    vista1.cmbTipos.setSelectedIndex(ventaEncontrada.getTipo() - 1);
                    vista1.lblPrecio.setText(String.valueOf(ventaEncontrada.getPrecio()));
                    vista1.txtCantidad.setText(String.valueOf(ventaEncontrada.getCantidad()));
                    vista1.lblTotal.setText(String.valueOf(ventaEncontrada.getTotal()));

                    vista1.txtCodigo.setEnabled(false);
                    vista1.txtCantidad.setEnabled(false);
                    vista1.cmbTipos.setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(vista1, "Venta no encontrada.");
                    habilitar();
                }
                this.Manejador.desconectar();
            } else {
                JOptionPane.showMessageDialog(vista1, "No se pudo establecer la conexión con la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista1, "Error al mostrar la venta: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void mostrarRegistrosEnTabla() {
        try {
            if (this.Manejador.conectar()) {
                ArrayList<Ventas> listaVentas = db.listar();
                DefaultTableModel modelo = new DefaultTableModel();
                modelo.addColumn("ID");
                modelo.addColumn("Código");
                modelo.addColumn("Tipo");
                modelo.addColumn("Precio");
                modelo.addColumn("Cantidad");
                modelo.addColumn("Total");

                for (Ventas venta : listaVentas) {
                    Object[] fila = {venta.getId(), venta.getCodigo(), venta.getTipo(), venta.getPrecio(), venta.getCantidad(), venta.getTotal()};
                    modelo.addRow(fila);
                }

                vista2.jtbRegistros.setModel(modelo);
                this.Manejador.desconectar();
            } else {
                JOptionPane.showMessageDialog(vista1, "No se pudo establecer la conexión con la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista1, "Error al mostrar las ventas: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) {
        dbVentas db = new dbVentas();
        dbManejador cot = new dbManejador();
        dlgVentas Ventas = new dlgVentas(new JFrame(), false);
        Ventas.setTitle("Registro de Ventas");
        Ventas.setSize(600, 600);
        Ventas.setVisible(false);

        dlgRegistros Registros = new dlgRegistros(new JFrame(), false);
        Registros.setTitle("Registros de Ventas");
        Registros.setSize(600, 400);
        Registros.setVisible(false);

        Ventas ventas = new Ventas();
        Controlador con = new Controlador(Ventas, Registros, ventas, cot, db);
        con.iniciarVista();
    }
}