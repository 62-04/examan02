/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
//CONECTAR BD
import java.sql.*;

public class dbManejador {
    //PROTECCION DE LA BASE
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registro;
    
    //VARIABLES DE LA CONEXION 
    private String usuario;
    private String dataBase;
    private String contraseña;
    private String drive;
    private String url;
    
    //CONSTRUCTOR DE PARAMETROS
    public dbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registro, String usuario, String dataBase, String contraseña, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registro = registro;
        this.usuario = usuario;
        this.dataBase = dataBase;
        this.contraseña = contraseña;
        this.drive = drive;
        this.url = url ;
        isDrive();
    }
    
    //CONSTRUCTOR VACIO
    public dbManejador() {
        this.drive = "com.mysql.cj.jdbc.Driver";
        this.usuario = "admin";
        this.contraseña="ramon2008";
        this.url="jdbc:mysql://microinformaticamx.cmwe0ss4z5wt.us-east-2.rds.amazonaws.com/sistema";     
        this.dataBase = "Sistema";
        isDrive();
    }    
    //METODOS GETTER & SETTER
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistro() {
        return registro;
    }

    public void setRegistro(ResultSet registro) {
        this.registro = registro;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDataBase() {
        return dataBase;
    }

    public void setDataBase(String dataBase) {
        this.dataBase = dataBase;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean isDrive(){
        boolean exito= false;
        try{
            Class.forName(drive);
           exito =true;
        }catch(ClassNotFoundException e){
            exito= false;
            System.err.println("Surgio un error -->> " + e.getMessage());
            System.exit(-1);
        }
        return exito;
    }
    
    public boolean conectar(){
        boolean exito = false;
        try{
            this.setConexion((DriverManager.getConnection(this.url,this.usuario,this.contraseña)));
            exito = true;
        }catch (SQLException e){
            exito = false;
            System.err.println("Surgio un error al conectar -->> " + e.getMessage());
            
        }
        return exito;
    }
    public void desconectar(){
        try{
            if(!this.conexion.isClosed()) this.getConexion().close();
        }catch(SQLException e){
            System.err.println("error No fue posible cerrar la conexion -->> "+ e.getMessage());
            
        }
       
    }
}