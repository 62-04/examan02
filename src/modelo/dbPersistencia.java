
package modelo;

import java.util.ArrayList;

public interface dbPersistencia {
        public void insertar(Object objecto)throws Exception;
        public ArrayList listar(String criterio)throws Exception;
        public Object buscar(String nombre) throws Exception;
}
