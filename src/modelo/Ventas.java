/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Juan Antonio
 */
public class Ventas {
    private int id;
    private String codigo;
    private int tipo;
    private double precio;
    private int cantidad;
    private double total;

    public Ventas(int id, String codigo, int tipo, double precio, int cantidad, double total) {
        this.id = id;
        this.codigo = codigo;
        this.tipo = tipo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.total = total;
    }
     public Ventas(Ventas ven) {
        this.id = ven.id;
        this.codigo = ven.codigo;
        this.tipo = ven.tipo;
        this.precio = ven.precio;
        this.cantidad = ven.cantidad;
        this.total = ven.total;
    }
    public Ventas() {
        this.id = 0;
        this.codigo = "";
        this.tipo = 0;
        this.precio = 0.0;
        this.cantidad = 0;
        this.total = 0.0;
    }

    public float getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }    
    
}
