
package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public class dbVentas extends dbManejador implements dbPersistencia {
    
    public void insertar(Object objeto) throws Exception {
        Ventas ven = (Ventas) objeto;
        String consulta = "insert into ventas (codigo, tipo, precio, cantidad, total) values (?, ?, ?, ?, ?)";
        if (this.conectar()) {
            try {
                System.err.println("Se conectó!!");
                this.sqlConsulta = conexion.prepareStatement(consulta);              
                // Asignar valores de la consulta
                this.sqlConsulta.setString(1, ven.getCodigo());
                this.sqlConsulta.setInt(2, ven.getTipo());
                this.sqlConsulta.setDouble(3, ven.getPrecio());
                this.sqlConsulta.setInt(4, ven.getCantidad());
                this.sqlConsulta.setDouble(5, ven.getTotal());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("Surgió un ERROR al INSERTAR --->> " + e.getMessage());
            }
        }
    }
    public ArrayList<Ventas> listar() throws Exception {
        ArrayList<Ventas> lista = new ArrayList<Ventas>();
        Ventas ven;
        if (this.conectar()) {
            String consulta = "Select * from ventas order by id";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registro = this.sqlConsulta.executeQuery();
            while (this.registro.next()) {
                ven = new Ventas();
                ven.setId(this.registro.getInt("id"));
                ven.setCodigo(this.registro.getString("codigo"));
                ven.setTipo(this.registro.getInt("tipo"));
                ven.setPrecio(this.registro.getDouble("precio"));
                ven.setCantidad(this.registro.getInt("cantidad"));
                ven.setTotal(this.registro.getDouble("total"));
                lista.add(ven); // Agregar el objeto Ventas a la lista
            }
        }
        this.desconectar();
        return lista;
    }
    public Object buscar(String nombre) throws Exception {
        Ventas ven = null; // Inicializar el objeto Ventas como nulo
        if (this.conectar()) {
            String consulta = "Select * from ventas where codigo = ?"; 
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, nombre); 
            this.registro = this.sqlConsulta.executeQuery();

            if (this.registro.next()) {
                ven = new Ventas();
                ven.setId(this.registro.getInt("id"));
                ven.setCodigo(this.registro.getString("codigo"));
                ven.setTipo(this.registro.getInt("tipo"));
                ven.setPrecio(this.registro.getDouble("precio"));
                ven.setCantidad(this.registro.getInt("cantidad"));
                ven.setTotal(this.registro.getDouble("total"));
            }
        }
        this.desconectar();
        return ven;
    }
    @Override
    public ArrayList listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void actualizar(Ventas venta) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
